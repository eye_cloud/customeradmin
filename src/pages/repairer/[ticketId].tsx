import MachineMapMarker from "@/components/MachineMapMarker";
import { Button } from "@/components/ui/button";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { TicketStatusTextColorStyles } from "@/lib/CustomTailwindStyles";
import { trpc } from "@/lib/trpc";
import { Status, Uuid } from "@/lib/types";
import { StatusSchema } from "@/server/schemas/ticket.schema";
import { GoogleMap, useJsApiLoader } from "@react-google-maps/api";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

const StatusSelectItems = () => {
  const TicketStatusTexts = Array.from(TicketStatusTextColorStyles.entries());

  return (
    <>
      {TicketStatusTexts.map(([key, value]) => (
        <SelectItem key={key} value={key} className={value}>
          {key}
        </SelectItem>
      ))}
    </>
  );
};

const gMapDefaultProps = {
  center: {
    lat: 24.78553,
    lng: 120.9979,
  },
  zoom: 16,
};

const gMapDirectionBaseUrl = "https://www.google.com/maps/dir/?api=1";
const NavigationButton = ({ machineId }: { machineId: Uuid }) => {
  const machineQuery = trpc.machine.getMachine.useQuery({ id: machineId });
  const machine = machineQuery.data;
  if (machine === undefined) return <></>;

  const machineLocation = machine.location.replace("POINT(", "").slice(0, -1);

  const lng = +machineLocation.split(" ")[0];
  const lat = +machineLocation.split(" ")[1];

  return (
    <a
      href={`${gMapDirectionBaseUrl}&destination=${lat},${lng}`}
      className="w-full"
    >
      <Button className="w-full">Navigation</Button>
    </a>
  );
};

const TicketView = ({ id }: { id: number }) => {
  const ticketQuery = trpc.ticket.getTicket.useQuery({ id });
  const ticket = ticketQuery.data;
  const ticketUpdateMutation = trpc.ticket.updateTicket.useMutation();

  const [statusSelectValue, setStatusSelectValue] = useState<Status>("OPEN");

  useEffect(() => {
    if (ticket?.status) setStatusSelectValue(ticket?.status);
  }, [ticket]);

  const handleUpdateStatus = (value: string) => {
    const status = StatusSchema.parse(value);
    ticketUpdateMutation.mutate({ id, status });
    setStatusSelectValue(status);
  };

  if (ticket === undefined) return <></>;

  return (
    <>
      <div className="absolute left-0 top-0 h-screen w-screen">
        <GoogleMap
          id="gmap"
          mapContainerClassName="absolute left-0 top-0 h-screen w-screen"
          zoom={gMapDefaultProps.zoom}
          center={gMapDefaultProps.center}
          options={{
            disableDefaultUI: true,
          }}
        >
          <MachineMapMarker machineId={ticket.machineId} />
        </GoogleMap>
      </div>
      <div className="absolute bottom-0 w-full rounded-t-2xl bg-slate-50 p-4">
        <div className="flex flex-row items-center justify-between">
          <h1 className="scroll-m-20 text-4xl font-extrabold tracking-tight lg:text-5xl">
            #{ticket.id}
          </h1>
          <Select value={statusSelectValue} onValueChange={handleUpdateStatus}>
            <SelectTrigger className="w-fit">
              <SelectValue placeholder="Status" />
            </SelectTrigger>
            <SelectContent>
              <StatusSelectItems />
            </SelectContent>
          </Select>
        </div>
        <div className="mt-2 flex w-full flex-col gap-2 text-sm">
          <div className="grid grid-cols-5 gap-4">
            <span className="font-medium">Machine ID</span>
            <span className="col-start-2 col-end-5">{ticket.machineId}</span>
          </div>
          <div className="grid grid-cols-5 gap-4">
            <span className="font-medium">Part</span>
            <span className="col-start-2 col-end-5">{ticket.part}</span>
          </div>
          <div className="grid grid-cols-5 gap-4">
            <span className="font-medium">Assignee</span>
            <span className="col-start-2 col-end-5">
              {ticket.assignee.name}
            </span>
          </div>
          <NavigationButton machineId={ticket.machineId} />
        </div>
      </div>
    </>
  );
};

export default function RepairerTicket() {
  const { isLoaded } = useJsApiLoader({
    googleMapsApiKey: process.env.NEXT_PUBLIC_GMAP_API_KEY ?? "",
  });

  const router = useRouter();
  const { ticketId } = router.query;

  if (ticketId === undefined || !isLoaded) return <></>;

  return <TicketView id={+ticketId} />;
}
