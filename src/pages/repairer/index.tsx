import TicketListForRepairer from "@/components/TicketListForRepairer";
import { trpc } from "@/lib/trpc";
import { useRouter } from "next/router";

export default function RepairerLanding() {
  const router = useRouter();
  const { userId } = router.query;

  if (userId === undefined) return <></>;

  const ticketQuery = trpc.repairer.getTickets.useQuery({
    id: userId as string,
  });
  const ticket = ticketQuery.data;

  return (
    <div className="p-4">
      <div className="flex justify-between">
        <h2 className="scroll-m-20 text-3xl font-semibold tracking-tight transition-colors first:mt-0">
          Your Tickets
        </h2>
      </div>
      <TicketListForRepairer tickets={ticket} />
    </div>
  );
}
