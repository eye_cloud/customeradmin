import MachineMapMarker from "@/components/MachineMapMarker";
import ReportList from "@/components/ReportList";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import PanelLayout from "@/layouts/PanelLayout";
import { TicketStatusTextColorStyles } from "@/lib/CustomTailwindStyles";
import { trpc } from "@/lib/trpc";
import { DatabaseId, Status, Ticket } from "@/lib/types";
import { StatusSchema } from "@/server/schemas/ticket.schema";
import { CheckIcon, XIcon } from "lucide-react";
import { useRouter } from "next/router";
import { ChangeEvent, PropsWithoutRef, useEffect, useState } from "react";
import { z } from "zod";

const InputMachineId = ({
  id,
  machineId,
}: Pick<Ticket, "id" | "machineId">) => {
  const updateTicketMutation = trpc.ticket.updateTicket.useMutation();

  const handlerUpdate = (ev: ChangeEvent<HTMLInputElement>) => {
    const newMachineId = ev.target.value;
    updateTicketMutation.mutate({ id, machineId: newMachineId });
  };

  return (
    <>
      <Label htmlFor="machine-id">Machine ID</Label>
      <Input
        id="machine-id"
        className="col-start-2 col-end-5"
        onChange={handlerUpdate}
        defaultValue={machineId}
      />
    </>
  );
};

const InputPart = ({ id, part }: Pick<Ticket, "id" | "part">) => {
  const updateTicketMutation = trpc.ticket.updateTicket.useMutation();

  const handlerUpdate = (ev: ChangeEvent<HTMLInputElement>) => {
    const newPart = ev.target.value;
    updateTicketMutation.mutate({ id, part: newPart });
  };

  return (
    <>
      <Label htmlFor="part">Part</Label>
      <Input
        id="part"
        className="col-start-2 col-end-5"
        onChange={handlerUpdate}
        defaultValue={part}
      />
    </>
  );
};

const InputAssignee = ({ id, assignee }: Pick<Ticket, "id" | "assignee">) => {
  const updateTicketMutation = trpc.ticket.updateTicket.useMutation();
  const [inputValue, setInputValue] = useState(
    `${assignee?.id} ${assignee?.name}`
  );

  const userQuery = trpc.user.getUser.useQuery(
    { id: inputValue },
    { enabled: false }
  );

  const [changedState, setChangedState] = useState(false);
  const [inputErrorState, setInputErrorState] = useState(false);

  const handleUpdate = () => {
    userQuery.refetch();
    const newAssignee = userQuery.data;

    if (newAssignee === undefined) {
      setInputErrorState(true);
      return;
    }

    if (newAssignee.role === "Repairer") {
      updateTicketMutation.mutate({ id, assignee: newAssignee.id });
      setInputValue(`${newAssignee?.id} ${newAssignee?.name}`);
      setChangedState(false);
    }
  };

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.target.value);
    setChangedState(true);
    setInputErrorState(false);
  };

  const handleReset = () => {
    setInputValue(`${assignee?.id} ${assignee?.name}`);
    setChangedState(false);
    setInputErrorState(false);
  };

  return (
    <>
      <Label htmlFor="assignee">Assignee</Label>
      <div className="col-start-2 col-end-5 flex flex-row gap-2">
        <Input
          id="assignee"
          className={`col-start-2 col-end-5 flex-1 ${
            inputErrorState ? "border-red-500" : ""
          }`}
          onChange={handleChange}
          value={inputValue}
          placeholder="Not Assigned"
        />
        <div className={`flex flex-row gap-1 ${changedState ? "" : "hidden"}`}>
          <Button onClick={handleUpdate}>
            <CheckIcon />
          </Button>
          <Button onClick={handleReset}>
            <XIcon />
          </Button>
        </div>
      </div>
    </>
  );
};

const InputAssignAdmin = ({
  id,
  assignAdmin,
}: Pick<Ticket, "id" | "assignAdmin">) => {
  const updateTicketMutation = trpc.ticket.updateTicket.useMutation();
  const [inputValue, setInputValue] = useState(
    assignAdmin === null ? "" : [assignAdmin?.id, assignAdmin?.name].join(" ")
  );

  const userQuery = trpc.user.getUser.useQuery(
    { id: inputValue },
    { enabled: false }
  );

  const [changedState, setChangedState] = useState(false);
  const [inputErrorState, setInputErrorState] = useState(false);

  const handleUpdate = () => {
    userQuery.refetch();
    const newAssignee = userQuery.data;

    if (newAssignee === undefined) {
      setInputErrorState(true);
      return;
    }

    if (newAssignee.role === "Admin") {
      updateTicketMutation.mutate({ id, assignee: newAssignee.id });
      setInputValue(`${newAssignee?.id} ${newAssignee?.name}`);
      setChangedState(false);
    }
  };

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.target.value);
    setChangedState(true);
    setInputErrorState(false);
  };

  const handleReset = () => {
    setInputValue(`${assignAdmin?.id} ${assignAdmin?.name}`);
    setChangedState(false);
    setInputErrorState(false);
  };

  return (
    <>
      <Label htmlFor="assign-admin">Assign Admin</Label>
      <div className="col-start-2 col-end-5 flex flex-row gap-2">
        <Input
          id="assign-admin"
          className={`col-start-2 col-end-5 flex-1 ${
            inputErrorState ? "border-red-500" : ""
          }`}
          onChange={handleChange}
          value={inputValue}
          placeholder="Not Assigned"
        />
        <div className={`flex flex-row gap-1 ${changedState ? "" : "hidden"}`}>
          <Button onClick={handleUpdate}>
            <CheckIcon />
          </Button>
          <Button onClick={handleReset}>
            <XIcon />
          </Button>
        </div>
      </div>
    </>
  );
};

const InputGroup = ({ ticket }: { ticket?: Ticket }) => {
  if (ticket === undefined) return <></>;

  return (
    <>
      <div className="grid grid-cols-4 flex-wrap items-center gap-3">
        <InputMachineId id={ticket.id} machineId={ticket.machineId} />
      </div>
      <div className="grid grid-cols-4 flex-wrap items-center gap-3">
        <InputPart id={ticket.id} part={ticket.part} />
      </div>
      <div className="grid grid-cols-4 flex-wrap items-center gap-3">
        <InputAssignee id={ticket.id} assignee={ticket.assignee} />
      </div>
      <div className="grid grid-cols-4 flex-wrap items-center gap-3">
        <InputAssignAdmin id={ticket.id} assignAdmin={ticket.assignAdmin} />
      </div>
    </>
  );
};

const StatusSelectItems = () => {
  const TicketStatusTexts = Array.from(TicketStatusTextColorStyles.entries());

  return (
    <>
      {TicketStatusTexts.map(([key, value]) => (
        <SelectItem key={key} value={key} className={value}>
          {key}
        </SelectItem>
      ))}
    </>
  );
};

const TicketEditBody = ({ ticket }: PropsWithoutRef<{ ticket: Ticket }>) => {
  const reportsQuery = trpc.ticket.getTicketLinkedReport.useQuery({
    id: ticket.id,
  });
  const ticketUpdateMutation = trpc.ticket.updateTicket.useMutation();
  const reports = reportsQuery?.data;
  const [statusSelectValue, setStatusSelectValue] = useState<Status>("OPEN");

  useEffect(() => {
    if (ticket?.status) setStatusSelectValue(ticket?.status);
  }, [ticket]);

  const handleUpdateStatus = (value: string) => {
    const status = StatusSchema.parse(value);
    ticketUpdateMutation.mutate({ id: ticket.id, status });
    setStatusSelectValue(status);
  };

  return (
    <div className="flex flex-col gap-8">
      <div className="flex flex-row items-center justify-between">
        <h1 className="scroll-m-20 text-4xl font-extrabold tracking-tight lg:text-5xl">
          #{ticket?.id}
        </h1>
        <Select value={statusSelectValue} onValueChange={handleUpdateStatus}>
          <SelectTrigger className="w-fit">
            <SelectValue placeholder="Status" />
          </SelectTrigger>
          <SelectContent>
            <StatusSelectItems />
          </SelectContent>
        </Select>
      </div>
      <div className="flex flex-col gap-2">
        <InputGroup ticket={ticket} />
        <div className="text-left font-bold">Linked Reports</div>
        <ReportList reports={reports} />
      </div>
    </div>
  );
};

const Page = ({ ticketId }: PropsWithoutRef<{ ticketId: DatabaseId }>) => {
  const ticketQuery = trpc.ticket.getTicket.useQuery({ id: ticketId });
  const ticket = ticketQuery.data;

  if (ticket === undefined) return <div>Loading</div>;

  return (
    <>
      <PanelLayout
        variant="md"
        markers={<MachineMapMarker machineId={ticket.machineId} />}
      >
        <TicketEditBody ticket={ticket} />
      </PanelLayout>
    </>
  );
};

export default function TicketEditPage() {
  const router = useRouter();
  const rawId = router.query.id;
  const [ticketId, setTicketId] = useState<DatabaseId>();

  useEffect(() => {
    if (rawId === undefined) return;

    // ensure query is load than query ticket
    setTicketId(z.string().transform(Number).parse(rawId));
  }, [rawId]);

  if (ticketId === undefined) return <></>;

  return <Page ticketId={ticketId} />;
}
