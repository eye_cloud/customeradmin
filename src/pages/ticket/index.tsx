import MachineMapMarker from "@/components/MachineMapMarker";
import TicketList from "@/components/TicketList";
import PanelLayout from "@/layouts/PanelLayout";
import { trpc } from "@/lib/trpc";
import { ReactNode, useEffect, useState } from "react";

export default function TicketListView() {
  const ticketsQuery = trpc.ticket.getTickets.useQuery();
  const tickets = ticketsQuery.data;

  const [markers, setMarkers] = useState<ReactNode>(<></>);

  useEffect(() => {
    if (tickets === undefined) return;

    setMarkers(
      <>
        {tickets.map((ticket) => (
          <MachineMapMarker key={ticket.id} machineId={ticket.machineId} />
        ))}
      </>
    );
  }, [tickets]);

  return (
    <PanelLayout variant="md" id="machine-list" markers={markers}>
      <h1 className="scroll-m-20 text-4xl font-extrabold tracking-tight lg:text-5xl">
        Tickets
      </h1>
      <TicketList tickets={tickets} />
    </PanelLayout>
  );
}
