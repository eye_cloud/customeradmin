import MachineList from "@/components/MachineList";
import PanelLayout from "@/layouts/PanelLayout";
import { trpc } from "@/lib/trpc";

export default function Machine() {
  const machines = trpc.machine.getMachinesSortByTypes.useQuery();

  return (
    <PanelLayout variant="md" id="machine-list">
      <h1 className="scroll-m-20 text-4xl font-extrabold tracking-tight lg:text-5xl">
        Machine
      </h1>
      <MachineList list={machines.data} />
    </PanelLayout>
  );
}
