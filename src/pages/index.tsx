import TicketList from "@/components/TicketList";
import { Button } from "@/components/ui/button";
import { Progress } from "@/components/ui/progress";
import PanelLayout from "@/layouts/PanelLayout";
import { trpc } from "@/lib/trpc";
import { MapIcon, Settings } from "lucide-react";
import Link from "next/link";

const PercentageBar = ({
  name,
  percentage,
}: {
  name: string;
  percentage: number;
}) => (
  <div className="flex items-center gap-5">
    <h3 className="flex-1 scroll-m-20 whitespace-nowrap text-2xl font-semibold tracking-tight">
      {name}
    </h3>
    <Progress value={percentage} />
    <span className="text-xl text-muted-foreground">{percentage}</span>
  </div>
);

const LinkIconButton = ({
  name,
  icon,
  href,
}: {
  name: string;
  icon: JSX.Element;
  href: string;
}) => (
  <Link href={href} className="aspect-square h-fit">
    <Button variant="ghost" className="h-full w-full flex-col">
      {icon}
      <div className="whitespace-nowrap">{name}</div>
    </Button>
  </Link>
);

export default function Home() {
  const pendingTickets = trpc.ticket.getPendingTickets.useQuery();

  return (
    <PanelLayout variant="full">
      <h1 className="scroll-m-20 text-4xl font-extrabold tracking-tight lg:text-5xl">
        Good Morning
      </h1>
      <div className="grid grid-cols-8 gap-5 p-4">
        <div className="col-start-1 col-end-5 flex flex-col gap-5">
          <h2 className="scroll-m-20 text-3xl font-semibold tracking-tight transition-colors first:mt-0">
            Machine Online Percentage
          </h2>
          <PercentageBar name="MKE-310" percentage={30} />
          <PercentageBar name="MKE-310" percentage={30} />
          <PercentageBar name="MKE-310" percentage={30} />
          <PercentageBar name="MKE-310" percentage={30} />
        </div>
        <div className="col-start-5 col-end-8">
          <div className="flex justify-between">
            <h2 className="scroll-m-20 text-3xl font-semibold tracking-tight transition-colors first:mt-0">
              Pending Tickets
            </h2>
            <Link href="/ticket">
              <Button variant="link">All Tickets</Button>
            </Link>
          </div>
          <TicketList tickets={pendingTickets.data} />
        </div>
        <div className="col-start-8 col-end-9 flex flex-col gap-4">
          <LinkIconButton
            name="Map View"
            href="/machine"
            icon={<MapIcon className="h-9 w-9" />}
          />
          <LinkIconButton
            name="Settings"
            href="/settings"
            icon={<Settings className="h-9 w-9" />}
          />
        </div>
      </div>
    </PanelLayout>
  );
}
