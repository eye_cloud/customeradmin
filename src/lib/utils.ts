import { clsx, type ClassValue } from "clsx";
import _ from "lodash";
import { twMerge } from "tailwind-merge";
import { z } from "zod";

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}

export function getBaseUrl() {
  if (typeof window !== "undefined") return ""; // browser should use relative path;

  if (process.env.VERCEL_URL) return `https://${process.env.VERCEL_URL}`;

  if (process.env.INTERNAL_URL)
    return `http://${process.env.INTERNAL_URL}:${process.env.PORT ?? 3000}`;

  // localhost
  return `http://localhost:${process.env.PORT ?? 3000}`;
}

export function camelCaseKeysInObject(object: object) {
  return _.mapKeys(object, (value, key) => {
    return _.camelCase(key);
  });
}

export async function fetchJsonData(url: string) {
  z.string().url().parse(url);

  const response = await fetch(url);

  if (!response.ok)
    throw new Error(`Fetch Failed: ${response.status} ${response.statusText}`);

  return await response.json();
}
