import { MachineStatus, Status } from "./types";

export const TicketStatusTextColorStyles = new Map<Status, string>([
  ["OPEN", "text-green-800"],
  ["WIP", "text-amber-700"],
  ["CLOSED", "text-indigo-600"],
]);

export const ReportStatusTextColorStyles = new Map<Status, string>([
  ["OPEN", "text-green-800"],
  ["WIP", "text-amber-700"],
  ["CLOSED", "text-indigo-600"],
]);

export const MachineStatusTextColorStyles = new Map<MachineStatus, string>([
  ["Online", "text-emerald-600"],
  ["Broken", "text-rose-600"],
  ["Repairing", "text-amber-600"],
  ["Offline", "text-stone-400"],
]);
