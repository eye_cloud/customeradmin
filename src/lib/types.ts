export type DatabaseId = number;
export type Uuid = string;
export type StringId = string;
export type Email = string;
export type Phone = string;
export type Name = string;

export type Role = "Admin" | "Repairer";
export type Person = {
  id: Uuid;
  name: Name;
  email: Email;
  phone: Phone;
  role?: Role;
};
export type PersonMeta = Pick<Person, "id">;

export type Admin = Person & { role: "Admin" };
export type Repairer = Person & { role: "Repairer" };

export type GetPerson = (meta: PersonMeta) => Promise<Person>;

export type Status = "OPEN" | "WIP" | "CLOSED";

export type MachinePart = string;

export type Ticket = {
  id: DatabaseId;
  machineId: Uuid;
  part: MachinePart;
  assignee: Repairer;
  status: Status;
  assignAdmin: Admin | null;
};
export type TicketMeta = Pick<Ticket, "id">;
export type TicketPreview = Omit<Ticket, "assignAdmin">;

export type GetTicket = (meta: TicketMeta) => Ticket | undefined;
export type GetPendingTickets = () => Ticket[];
export type GetAllTickets = () => Ticket[];
export type GetTicketLinkedReports = (meta: TicketMeta) => Report[];
export type PermitTicket = (meta: TicketMeta, assignAdmin: Admin) => Boolean;

export type Description = string;
export type OtherRemark = string;
export type Report = {
  id: DatabaseId;
  status: Status;
  description: Description;
  machineId: Uuid;
};
export type ReportMeta = Pick<Report, "id">;

export type GetReport = (meta: ReportMeta) => Report;
export type GetAllReports = () => Report[];
export type GetReportLinkedTicket = (meta: ReportMeta) => Ticket;

export type MachineType = string;
export type MahcineModel = string;
export type MachineStatus = "Online" | "Broken" | "Repairing" | "Offline";
export type Location = string;

export type Machine = {
  id: Uuid;
  textId: StringId;
  type: MachineType;
  status: MachineStatus;
  location: Location;
};
export type MachineMeta = Pick<Machine, "id">;

export type GetMachine = (meta: MachineMeta) => Machine;
export type GetAllMachines = () => Machine[];
export type GetMachineLinkedTickets = (meta: MachineMeta) => Ticket[];
