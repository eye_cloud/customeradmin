describe("machine list view spec", () => {
  beforeEach(() => {
    cy.visit("/machine");
  });

  it("user should be able to enter machine detail view when click on list item", () => {
    cy.get("#machine-list").should("be.visible");

    cy.get("#machine-list").get("button").first().click();

    cy.get("#machine-list").get("form").get("a").first().click();

    cy.location("pathname").should("eq", "/machine/13913-1395-1");
    cy.get("#machine-detail").should("be.visible");
  });
});

describe("machine detail view spec", () => {
  beforeEach(() => {
    cy.visit("/machine/13913-1395-1");
  });

  it("user should be able to close machine detail view when click on collapse button", () => {
    cy.get("#machine-detail section button").first().click();

    cy.get("#machine-detail").should("not.exist");
  });

  it("user should be able to see ticket detail in machine detail view", () => {
    cy.get("#machine-detail #ticket-list").should("be.visible");
  });
});
