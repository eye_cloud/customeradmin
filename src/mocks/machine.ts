const machineList = [
  {
    model: "model-10",
    machines: [
      {
        id: "MKE-310",
        uuid: "13913-1395-1",
        type: "e-scooter",
        model: "model-10",
        siteUuid: "31303031000000000000000000000000",
        status: "Online",
      },
    ],
  },
  {
    model: "model-9",
    machines: [
      {
        id: "MKE-210",
        uuid: "13923-1395-1",
        type: "e-scooter",
        model: "model-10",
        siteUuid: "31303031000000000000000000000000",
        status: "Online",
      },
    ],
  },
];

const machineDetail = new Map([
  [
    "13913-1395-1",
    {
      uuid: "13913-1395-1",
      machineId: "MKE-310",
      type: "scooter",
      model: "model-10",
      location: "123.462202, 41.804471",
      status: "Online",
    },
  ],
  [
    "13923-1395-1",
    {
      uuid: "13923-1395-1",
      machineId: "MKE-210",
      type: "scooter",
      model: "model-9",
      location: "123.462202, 41.804471",
      status: "Online",
    },
  ],
]);

export function getMachineList() {
  return machineList;
}

export function getMachineDetail(uuid: string) {
  return machineDetail.get(uuid);
}
