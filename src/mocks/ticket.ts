import type {
  GetAllTickets,
  GetPendingTickets,
  GetTicket,
  PermitTicket,
  Ticket,
} from "@/lib/types";

const Tickets: Ticket[] = [
  {
    id: 11,
    machineId: "13923-1395-1",
    part: "wheel",
    assignee: {
      id: "010240",
      name: "Aiden",
      email: "aiden@example.com",
      phone: "0912345678",
      role: "Repairer",
    },
    status: "OPEN",
    assignAdmin: null,
  },
  {
    id: 12,
    machineId: "13923-1395-1",
    part: "wheel",
    assignee: {
      id: "010240",
      name: "Aiden",
      email: "aiden@example.com",
      phone: "0912345678",
      role: "Repairer",
    },
    status: "OPEN",
    assignAdmin: null,
  },
  {
    id: 13,
    machineId: "13923-1395-1",
    part: "wheel",
    assignee: {
      id: "010240",
      name: "Aiden",
      email: "aiden@example.com",
      phone: "0912345678",
      role: "Repairer",
    },
    status: "OPEN",
    assignAdmin: null,
  },
  {
    id: 14,
    machineId: "13923-1395-1",
    part: "wheel",
    assignee: {
      id: "010240",
      name: "aiden",
      email: "aiden@example.com",
      phone: "0912345678",
      role: "Repairer",
    },
    status: "OPEN",
    assignAdmin: {
      id: "012045",
      name: "michael",
      email: "michael@example.com",
      phone: "0912345678",
      role: "Admin",
    },
  },
  {
    id: 15,
    machineId: "13923-1395-1",
    part: "wheel",
    assignee: {
      id: "010240",
      name: "aiden",
      email: "aiden@example.com",
      phone: "0912345678",
      role: "Repairer",
    },
    status: "WIP",
    assignAdmin: {
      id: "012045",
      name: "michael",
      email: "michael@example.com",
      phone: "0912345678",
      role: "Admin",
    },
  },
  {
    id: 16,
    machineId: "13923-1395-1",
    part: "wheel",
    assignee: {
      id: "010240",
      name: "aiden",
      email: "aiden@example.com",
      phone: "0912345678",
      role: "Repairer",
    },
    status: "CLOSED",
    assignAdmin: {
      id: "012045",
      name: "michael",
      email: "michael@example.com",
      phone: "0912345678",
      role: "Admin",
    },
  },
];

export const getPendingTickets: GetPendingTickets = () =>
  Tickets.filter((ticket) => ticket.assignAdmin === undefined);

export const getTickets: GetAllTickets = () => Tickets;

export const getTicket: GetTicket = ({ id }) => {
  return getTickets().find((ticket) => ticket.id === id);
};

export const permitTicket: PermitTicket = ({ id }, assignAdmin) => {
  const ticket = Tickets.find((ticket) => ticket.id === id);

  if (ticket === undefined) return false;

  ticket.assignAdmin = assignAdmin;
  return true;
};
