import { z } from "zod";
import { StringIdSchema, UuidSchema } from "./global.schema";

/* Inputs */
export const ParamsInputSchema = z.object({
  id: UuidSchema,
});
export type ParamsInput = z.TypeOf<typeof ParamsInputSchema>;

/* Outputs */
export const MachineStatusSchema = z.union([
  z.literal("Online"),
  z.literal("Broken"),
  z.literal("Repairing"),
  z.literal("Offline"),
]);

export const MachineSchema = z.object({
  id: UuidSchema,
  textId: StringIdSchema,
  type: z.string(),
  status: MachineStatusSchema,
  location: z.string(),
});
export type Machine = z.TypeOf<typeof MachineSchema>;

export const machinesSortByModel = z.array(
  z.object({
    type: z.string(),
    machines: MachineSchema.array(),
  })
);
export type MachinesSortByModel = z.TypeOf<typeof machinesSortByModel>;
