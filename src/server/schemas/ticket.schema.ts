import { z } from "zod";
import {
  AdminSchema,
  DatabaseIdSchema,
  RepairerSchema,
  UuidSchema,
} from "./global.schema";

export const StatusSchema = z.union([
  z.literal("OPEN"),
  z.literal("WIP"),
  z.literal("CLOSED"),
]);

export const MachinePartSchema = z.string();

export const RawTicketSchema = z.object({
  id: DatabaseIdSchema,
  machineId: UuidSchema,
  part: MachinePartSchema,
  assignee: UuidSchema,
  status: StatusSchema,
  assignAdmin: UuidSchema.nullable(),
});
export type RawTicket = z.TypeOf<typeof RawTicketSchema>;

export const TicketSchema = z.object({
  id: DatabaseIdSchema,
  machineId: UuidSchema,
  part: MachinePartSchema,
  assignee: RepairerSchema,
  status: StatusSchema,
  assignAdmin: AdminSchema.nullable(),
});
export const TicketMetaSchema = TicketSchema.pick({ id: true });

export const TicketUpdateBodySchema = RawTicketSchema.partial({
  machineId: true,
  part: true,
  assignee: true,
  status: true,
  assignAdmin: true,
});
export type TicketUpdateBody = z.TypeOf<typeof TicketUpdateBodySchema>;

export const ReportDescriptionSchema = z.string();
export const ReportOtherRemarkSchema = z.string();
export const ReportSchema = z.object({
  id: DatabaseIdSchema,
  status: StatusSchema,
  description: ReportDescriptionSchema,
  machineId: UuidSchema,
});

export const ParamsInputSchema = z.object({
  id: DatabaseIdSchema,
});
export type ParamsInput = z.TypeOf<typeof ParamsInputSchema>;
