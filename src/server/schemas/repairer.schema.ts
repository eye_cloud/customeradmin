import { z } from "zod";

export const ParamsInputSchema = z.object({
  id: z.string(),
});
export type ParamsInput = z.TypeOf<typeof ParamsInputSchema>;
