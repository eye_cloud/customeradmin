import { z } from "zod";
import { DatabaseIdSchema } from "./global.schema";

export const ParamsInputSchema = z.object({
  id: DatabaseIdSchema,
});
export type ParmasInput = z.TypeOf<typeof ParamsInputSchema>;
