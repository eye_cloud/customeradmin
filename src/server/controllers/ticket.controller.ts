import { fetchJsonData } from "@/lib/utils";
import { getPendingTickets, getTicket, getTickets } from "@/mocks/ticket";
import { TRPCError } from "@trpc/server";
import {
  ParamsInput,
  RawTicketSchema,
  TicketSchema,
  TicketUpdateBody,
} from "../schemas/ticket.schema";
import { parsePerson, parsePersonInTickets } from "../services/ticket.service";

const inTesting = false;
const baseUrl = process.env.BACKEND_URL;
const mockAssignAdmin = "38313132-3039-0000-0000-000000000000";

export const getPendingTicketsHandler = async () => {
  if (inTesting) {
    return getPendingTickets();
  }

  const data = await fetchJsonData(`${baseUrl}/ticket/pending_list`);

  const rawTickets = RawTicketSchema.array().parse(data.ticket_list);

  const tickets = TicketSchema.array().parse(
    await parsePersonInTickets(rawTickets)
  );

  return tickets;
};

export const getTicketsHandler = async () => {
  if (inTesting) {
    return getTickets();
  }

  const data = await fetchJsonData(`${baseUrl}/ticket/list`);

  const rawTickets = RawTicketSchema.array().parse(data.ticket_list);

  const tickets = TicketSchema.array().parse(
    await parsePersonInTickets(rawTickets)
  );

  return tickets;
};

export const getTicketHandler = async ({
  paramsInput,
}: {
  paramsInput: ParamsInput;
}) => {
  if (inTesting) {
    return getTicket({ id: paramsInput.id });
  }

  const ticketId = paramsInput.id;

  const data = await fetchJsonData(`${baseUrl}/ticket/${ticketId}`);

  const rawTicket = RawTicketSchema.parse(data.ticket);

  try {
    const ticket = TicketSchema.parse(await parsePerson(rawTicket));
    return ticket;
  } catch {
    throw new TRPCError({ code: "NOT_FOUND", message: "ticket not found" });
  }
};

export const permitTicketHandler = async ({
  paramsInput,
}: {
  paramsInput: ParamsInput;
}) => {
  if (inTesting) return;

  const ticketId = paramsInput.id;

  const response = await fetch(`${baseUrl}/ticket`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      id: ticketId,
      assignAdmin: mockAssignAdmin,
    }),
  });

  if (response.status === 422)
    throw new TRPCError({ code: "UNPROCESSABLE_CONTENT" });
};

export const updateTicketHandler = async ({
  updateBody,
}: {
  updateBody: TicketUpdateBody;
}) => {
  if (inTesting) return;

  const response = await fetch(`${baseUrl}/ticket`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(updateBody),
  });

  if (response.status === 422)
    throw new TRPCError({ code: "UNPROCESSABLE_CONTENT" });
};

export const getTicketLinkedReport = async ({
  paramsInput,
}: {
  paramsInput: ParamsInput;
}) => {
  const { id } = paramsInput;

  const data = await fetchJsonData(`${baseUrl}/ticket/linked_report/${id}`);

  return data.report_list;
};
