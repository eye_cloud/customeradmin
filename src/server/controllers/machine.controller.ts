import { Machine } from "@/lib/types";
import { camelCaseKeysInObject, fetchJsonData } from "@/lib/utils";
import _ from "lodash";
import {
  MachineSchema,
  ParamsInput,
  type MachinesSortByModel,
} from "../schemas/machine.schema";

const baseUrl = process.env.BACKEND_URL;
const inTesting = true;

const sortMachineByTypes = (machine: Machine[]) =>
  _.chain(machine)
    .groupBy("type")
    .map((value, key) => ({ type: key, machines: value }))
    .value();

export const getMachinesSortByTypesHandler =
  async (): Promise<MachinesSortByModel> => {
    const request = await fetch(`${baseUrl}/machine/list`);
    const data = await request.json();
    const machineList = _.map(data.machine_list, camelCaseKeysInObject);

    const machines = MachineSchema.array().parse(machineList);

    const machinesSortByTypes = sortMachineByTypes(machines);

    return machinesSortByTypes;
  };

export const getMachineHandler = async ({
  paramsInput,
}: {
  paramsInput: ParamsInput;
}): Promise<Machine> => {
  const data = await fetchJsonData(`${baseUrl}/machine/${paramsInput.id}`);
  const machine = data.machine;

  return machine;
};
