import { fetchJsonData } from "@/lib/utils";
import { ParamsInput } from "../schemas/repairer.schema";
import { RawTicketSchema, TicketSchema } from "../schemas/ticket.schema";
import { parsePersonInTickets } from "../services/ticket.service";

const baseUrl = process.env.BACKEND_URL;

export const getRepairerTicketHandler = async ({
  paramsInput,
}: {
  paramsInput: ParamsInput;
}) => {
  const personId = paramsInput.id;

  const data = await fetchJsonData(`${baseUrl}/person/${personId}/get_tickets`);

  const rawTickets = RawTicketSchema.array().parse(data.ticket_list);

  const tickets = TicketSchema.array().parse(
    await parsePersonInTickets(rawTickets)
  );

  return tickets;
};
