import { getReport, getReports } from "@/mocks/report";
import { ParmasInput } from "../schemas/report.schema";

const backendURL = process.env.BACKEND_URL;
const inTesting = true;

export const getReportsHandler = () => {
  if (inTesting) {
    return getReports();
  }
};

export const getNewReportsHandler = () => {
  if (inTesting) {
    return getReports();
  }
};

export const getReportHandler = ({
  paramsInput,
}: {
  paramsInput: ParmasInput;
}) => {
  if (inTesting) {
    return getReport({ id: paramsInput.id });
  }
};
