import { ParamsInput } from "../schemas/person.schema";
import { getPerson } from "../services/person.service";

const inTesting = false;
const baseUrl = process.env.BACKEND_URL;

export const getPersonHandler = ({
  paramsInput,
}: {
  paramsInput: ParamsInput;
}) => {
  const { id } = paramsInput;

  return getPerson({ id });
};
