import { GetPerson } from "@/lib/types";
import { PersonSchema } from "../schemas/global.schema";

const baseUrl = process.env.BACKEND_URL;

export const getPerson: GetPerson = async ({ id }) => {
  const response = await fetch(`${baseUrl}/person/${id}`);
  const data = await response.json();

  const person = PersonSchema.parse(data.person);

  return person;
};
