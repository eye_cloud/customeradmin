import { AdminSchema, RepairerSchema } from "../schemas/global.schema";
import { RawTicket } from "../schemas/ticket.schema";
import { getPerson } from "./person.service";

export const parsePerson = async (ticket: RawTicket) => {
  try {
    const assignee = RepairerSchema.parse(
      await getPerson({ id: ticket.assignee })
    );

    const assignAdmin = ticket?.assignAdmin
      ? AdminSchema.parse(await getPerson({ id: ticket.assignAdmin }))
      : null;

    return {
      id: ticket.id,
      status: ticket.status,
      machineId: ticket.machineId,
      part: ticket.part,
      assignee,
      assignAdmin,
    };
  } catch (error) {
    console.log(error);
    console.log("Invalid person schema");
  }
};

export const parsePersonInTickets = async (tickets: RawTicket[]) => {
  const result = await Promise.all(tickets.map(parsePerson));

  return result.filter((undefinableTicket) => undefinableTicket !== undefined);
};
