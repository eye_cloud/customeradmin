import {
  getNewReportsHandler,
  getReportHandler,
  getReportsHandler,
} from "../controllers/report.controller";
import { ParamsInputSchema } from "../schemas/report.schema";
import { procedure, router } from "../trpc";

export const reportRouter = router({
  getReports: procedure.query(() => getReportsHandler()),
  getNewReports: procedure.query(() => getNewReportsHandler()),
  getReport: procedure
    .input(ParamsInputSchema)
    .query(({ input }) => getReportHandler({ paramsInput: input })),
});
