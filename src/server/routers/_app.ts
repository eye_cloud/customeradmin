import { procedure, router } from "../trpc";
import { machineRouter } from "./machine";
import { repairerRouter } from "./repairer";
import { reportRouter } from "./report";
import { ticketRouter } from "./ticket";
import { userRouter } from "./user";

export const appRouter = router({
  greeting: procedure.query(async () => "hi"),
  machine: machineRouter,
  report: reportRouter,
  ticket: ticketRouter,
  user: userRouter,
  repairer: repairerRouter,
});

export type AppRouter = typeof appRouter;
