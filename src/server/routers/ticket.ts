import {
  getPendingTicketsHandler,
  getTicketHandler,
  getTicketLinkedReport,
  getTicketsHandler,
  permitTicketHandler,
  updateTicketHandler,
} from "../controllers/ticket.controller";
import {
  ParamsInputSchema,
  ReportSchema,
  TicketSchema,
  TicketUpdateBodySchema,
} from "../schemas/ticket.schema";
import { procedure, router } from "../trpc";

export const ticketRouter = router({
  getPendingTickets: procedure
    .output(TicketSchema.array())
    .query(() => getPendingTicketsHandler()),
  getTickets: procedure
    .output(TicketSchema.array())
    .query(() => getTicketsHandler()),
  getTicket: procedure
    .input(ParamsInputSchema)
    .query(({ input }) => getTicketHandler({ paramsInput: input })),
  permitTicket: procedure
    .input(ParamsInputSchema)
    .mutation(({ input }) => permitTicketHandler({ paramsInput: input })),
  updateTicket: procedure
    .input(TicketUpdateBodySchema)
    .mutation(({ input }) => updateTicketHandler({ updateBody: input })),
  getTicketLinkedReport: procedure
    .input(ParamsInputSchema)
    .output(ReportSchema.array())
    .query(({ input }) => getTicketLinkedReport({ paramsInput: input })),
});
