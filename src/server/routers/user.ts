import { getPersonHandler } from "../controllers/user.controller";
import { PersonSchema } from "../schemas/global.schema";
import { ParamsInputSchema } from "../schemas/person.schema";
import { procedure, router } from "../trpc";

export const userRouter = router({
  getUser: procedure
    .input(ParamsInputSchema)
    .output(PersonSchema)
    .query(({ input }) => getPersonHandler({ paramsInput: input })),
});
