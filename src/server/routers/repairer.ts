import { getRepairerTicketHandler } from "../controllers/repairer.controller";
import { ParamsInputSchema } from "../schemas/repairer.schema";
import { procedure, router } from "../trpc";

export const repairerRouter = router({
  getTickets: procedure
    .input(ParamsInputSchema)
    .query(({ input }) => getRepairerTicketHandler({ paramsInput: input })),
});
