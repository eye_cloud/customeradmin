import {
  getMachineHandler,
  getMachinesSortByTypesHandler,
} from "../controllers/machine.controller";
import {
  MachineSchema,
  ParamsInputSchema,
  machinesSortByModel,
} from "../schemas/machine.schema";
import { procedure, router } from "../trpc";

export const machineRouter = router({
  getMachinesSortByTypes: procedure
    .output(machinesSortByModel)
    .query(() => getMachinesSortByTypesHandler()),
  getMachine: procedure
    .input(ParamsInputSchema)
    .output(MachineSchema)
    .query(({ input }) => getMachineHandler({ paramsInput: input })),
});
