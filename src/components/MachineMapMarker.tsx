import { trpc } from "@/lib/trpc";
import { Uuid } from "@/lib/types";
import { MarkerF } from "@react-google-maps/api";
import { PropsWithoutRef } from "react";

export default function MachineMapMarker({
  machineId,
}: PropsWithoutRef<{ machineId: Uuid }>) {
  const machineQuery = trpc.machine.getMachine.useQuery({ id: machineId });
  const machine = machineQuery.data;

  if (machine === undefined) return <></>;

  const machineLocation = machine.location.replace("POINT(", "").slice(0, -1);
  const markerPosition = {
    lng: +machineLocation.split(" ")[0],
    lat: +machineLocation.split(" ")[1],
  };

  return <MarkerF position={markerPosition} />;
}
