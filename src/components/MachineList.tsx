import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from "@/components/ui/accordion";
import { MachineStatusTextColorStyles } from "@/lib/CustomTailwindStyles";
import { Machine } from "@/lib/types";
import type { MachinesSortByModel } from "@/server/schemas/machine.schema";
import Link from "next/link";
import { useRouter } from "next/router";
import { Label } from "./ui/label";
import { Skeleton } from "./ui/skeleton";

const ListItem = (props: { machineList: Machine[] }) => {
  const { machineList } = props;

  const router = useRouter();

  const radioGroupItems = machineList.map((machine) => {
    const machineStatusTextStyle = MachineStatusTextColorStyles.get(
      machine.status
    );

    return (
      <div
        key={machine.id}
        className="flex h-fit flex-row border-b border-b-slate-200 last:border-0"
      >
        <Link href={`${router.pathname}/${machine.id}`}>
          <input
            type="radio"
            value={machine.id}
            id={machine.id}
            name="radio-group__machine"
            className="peer invisible h-0 w-0"
          />
          <Label
            htmlFor={machine.id}
            className="flex w-full gap-2 bg-inherit p-2 text-sm peer-checked:bg-slate-200"
          >
            <span>{machine.id}</span>
            <span className={`${machineStatusTextStyle}`}>
              {machine.status}
            </span>
          </Label>
        </Link>
      </div>
    );
  });

  return (
    <AccordionContent className="px-4">
      <form>{radioGroupItems}</form>
    </AccordionContent>
  );
};

export default function MachineList(props: {
  list?: MachinesSortByModel;
  className?: string;
}) {
  const { list } = props;

  if (list === undefined) {
    return (
      <Accordion type="single" className={props.className}>
        <AccordionItem value="skeleton" className="py-4">
          <Skeleton className="h-6 w-full" />
        </AccordionItem>
        <AccordionItem value="skeleton" className="py-4">
          <Skeleton className="h-6 w-full" />
        </AccordionItem>
        <AccordionItem value="skeleton" className="py-4">
          <Skeleton className="h-6 w-full" />
        </AccordionItem>
      </Accordion>
    );
  }

  const accrodionItems = list.map((modelListItem) => (
    <AccordionItem key={modelListItem.type} value={modelListItem.type}>
      <AccordionTrigger className="font-medium">
        <span className="flex gap-2">
          <span className="font-medium text-slate-800">
            {modelListItem.type}
          </span>
        </span>
      </AccordionTrigger>
      <ListItem machineList={modelListItem.machines} />
    </AccordionItem>
  ));

  return (
    <Accordion type="single" collapsible className={props.className}>
      {accrodionItems}
    </Accordion>
  );
}
