// NOTE: Drunk Code

import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from "@/components/ui/accordion";
import { TicketStatusTextColorStyles } from "@/lib/CustomTailwindStyles";
import type { Ticket } from "@/lib/types";
import Link from "next/link";
import { Button } from "./ui/button";
import { Skeleton } from "./ui/skeleton";

const ListItem = (props: { ticket: Ticket }) => {
  const { ticket } = props;

  const ActionRow = () => {
    return (
      <div className="flex flex-row gap-2">
        <Link href={`/repairer/${ticket.id}`} className="flex-1">
          <Button variant="outline" className="w-full">
            Detail
          </Button>
        </Link>
      </div>
    );
  };

  return (
    <AccordionContent>
      <div className="w-full text-sm">
        <div className="grid grid-cols-5 gap-4">
          <span className="font-medium">Machine ID</span>
          <span className="col-start-2 col-end-5">{ticket.machineId}</span>
        </div>
        <div className="grid grid-cols-5 gap-4">
          <span className="font-medium">Part</span>
          <span className="col-start-2 col-end-5">{ticket.part}</span>
        </div>
        <div className="grid grid-cols-5 gap-4">
          <span className="font-medium">Assignee</span>
          <span className="col-start-2 col-end-5">{ticket.assignee.name}</span>
        </div>
        <div className="mt-2">
          <ActionRow />
        </div>
      </div>
    </AccordionContent>
  );
};

export default function TicketList(props: { tickets?: Ticket[] }) {
  const { tickets } = props;

  if (tickets === undefined) {
    return (
      <Accordion type="single">
        <AccordionItem value="skeleton" className="py-4">
          <Skeleton className="h-6 w-full" />
        </AccordionItem>
        <AccordionItem value="skeleton" className="py-4">
          <Skeleton className="h-6 w-full" />
        </AccordionItem>
        <AccordionItem value="skeleton" className="py-4">
          <Skeleton className="h-6 w-full" />
        </AccordionItem>
      </Accordion>
    );
  }

  const accrodionItems = tickets.map((ticket) => {
    const statusTextColorStyle = TicketStatusTextColorStyles.get(ticket.status);

    return (
      <AccordionItem key={ticket.id} value={String(ticket.id)}>
        <AccordionTrigger className="font-medium">
          <span className="flex gap-2">
            <span className="font-medium text-slate-500">#{ticket.id}</span>
            <span className={`${statusTextColorStyle}`}>{ticket.status}</span>
          </span>
        </AccordionTrigger>
        <ListItem ticket={ticket} />
      </AccordionItem>
    );
  });

  return (
    <Accordion type="single" collapsible>
      {accrodionItems}
    </Accordion>
  );
}
