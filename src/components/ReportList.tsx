import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from "@/components/ui/accordion";
import { ReportStatusTextColorStyles } from "@/lib/CustomTailwindStyles";
import { Report } from "@/lib/types";
import { Skeleton } from "./ui/skeleton";

const ListItem = (props: Pick<Report, "description" | "machineId">) => {
  const { machineId, description } = props;

  return (
    <AccordionContent>
      <div className="w-full text-sm">
        <div className="grid grid-cols-5 gap-4">
          <span className="font-medium">Machine ID</span>
          <span className="col-start-2 col-end-5">{machineId}</span>
        </div>
        <div className="grid grid-cols-5 gap-4">
          <span className="font-medium">Description</span>
          <span className="col-start-2 col-end-5">{description}</span>
        </div>
      </div>
    </AccordionContent>
  );
};

export default function ReportList(props: { reports?: Report[] }) {
  const { reports } = props;

  if (reports === undefined) {
    return (
      <Accordion type="single">
        <AccordionItem value="skeleton" className="py-4">
          <Skeleton className="h-6 w-full" />
        </AccordionItem>
        <AccordionItem value="skeleton" className="py-4">
          <Skeleton className="h-6 w-full" />
        </AccordionItem>
        <AccordionItem value="skeleton" className="py-4">
          <Skeleton className="h-6 w-full" />
        </AccordionItem>
      </Accordion>
    );
  }

  const accrodionItems = reports.map((report) => {
    const statusTextColorStyle = ReportStatusTextColorStyles.get(report.status);

    return (
      <AccordionItem key={report.id} value={String(report.id)}>
        <AccordionTrigger className="font-medium">
          <span className="flex gap-2">
            <span className="font-medium text-slate-500">#{report.id}</span>
            <span className={`${statusTextColorStyle}`}>{report.status}</span>
          </span>
        </AccordionTrigger>
        <ListItem
          description={report.description}
          machineId={report.machineId}
        />
      </AccordionItem>
    );
  });

  return (
    <Accordion type="single" collapsible>
      {accrodionItems}
    </Accordion>
  );
}
