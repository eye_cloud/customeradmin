import { GoogleMap, useJsApiLoader } from "@react-google-maps/api";
import { ComponentPropsWithoutRef, ReactNode } from "react";

const variantStyles = new Map<string, { container: string; children: string }>([
  [
    "md",
    {
      container: "grid h-screen w-screen grid-cols-12 gap-4 bg-transparent p-8",
      children:
        "z-10 col-start-8 col-end-13 overflow-scroll rounded-2xl bg-slate-50 p-8",
    },
  ],
  [
    "lg",
    {
      container: "grid h-screen w-screen grid-cols-12 gap-4 bg-slate-950 p-8",
      children:
        "z-10 col-start-6 col-end-13 h-full overflow-scroll rounded-2xl bg-slate-50 px-4 py-8",
    },
  ],
  [
    "full",
    {
      container: "grid h-screen w-screen grid-cols-12 gap-4 bg-slate-950 p-8",
      children:
        "z-10 col-start-1 col-end-13 h-full overflow-scroll rounded-2xl bg-slate-50 p-8",
    },
  ],
]);

interface Props extends ComponentPropsWithoutRef<"main"> {
  variant?: "md" | "lg" | "full";
  markers?: ReactNode;
}

export default function PanelLayout({
  children,
  variant = "md",
  markers,
  ...props
}: Props) {
  const { isLoaded } = useJsApiLoader({
    googleMapsApiKey: process.env.NEXT_PUBLIC_GMAP_API_KEY ?? "",
  });

  const gMapDefaultProps = {
    center: {
      lat: 24.78553,
      lng: 120.9979,
    },
    zoom: 15,
  };

  const variantStyle = variantStyles.get(variant);

  if (!isLoaded) return <></>;

  return (
    <main {...props} className={variantStyle?.container}>
      <div className="absolute left-0 top-0 h-screen w-screen">
        <GoogleMap
          id="gmap"
          mapContainerClassName="absolute left-0 top-0 h-screen w-screen"
          zoom={gMapDefaultProps.zoom}
          center={gMapDefaultProps.center}
          options={{
            disableDefaultUI: true,
          }}
        >
          {markers}
        </GoogleMap>
      </div>
      <div className={variantStyle?.children}>{children}</div>
    </main>
  );
}
